# coding: utf-8
require 'rails_helper'

RSpec.describe 'Post API', type: :request do
  let(:user) { create(:user) }
  let!(:posts) { create_list(:post, 10, created_by: user.id) }
  let(:post_id) { posts.first.id }
  # make request authorized
  let(:headers) { valid_headers }
  let(:invaid_headers) { invalid_headers }

  # GET request for all posts
  describe 'GET /posts' do
    before { get '/posts' }

    it 'returns json' do
      expect(json).not_to be_empty
    end

    it 'returns posts' do
      expect(json.size).to eq(10)
    end

    it { expect(response).to have_http_status(200) }
  end

  # GET request for a single Post
  describe 'GET /posts/:id' do
    before { get "/posts/#{post_id}" }

    context 'when the record exists' do
      it 'returns json' do
        expect(json).not_to be_empty
      end

      it 'returns the post' do
        expect(json['id']).to eq(post_id)
      end

      it { expect(response).to have_http_status(200) }
    end

    context 'when the record doesn\'t exist' do
      let(:post_id) { 100 }

      it 'returns not found message' do
        expect(response.body).to match(/Couldn\'t find Post/)
      end

      it { expect(response).to have_http_status(404) }
    end
  end

  # POST request to create Post
  describe 'POST /posts' do
    let(:new_title) { 'A New Post' }
    let(:valid_attributes) { { title: new_title, content: 'Hello There', created_by: user.id } }

    context 'with invalid headers' do
      before { post '/posts', params: valid_attributes, headers: invalid_headers }

      it { expect(response).to have_http_status(422) }
    end

    context 'when the request is valid' do
      before { post '/posts', params: valid_attributes, headers: headers }

      it 'creates a post' do
        expect(json['title']).to eq(new_title)
      end

      it { expect(response).to have_http_status(201) }
    end

    context 'when the request is invalid' do
      before { post '/posts', params: { title: 'Just A Title' }, headers: headers }

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Content can\'t be blank/)
      end

      it { expect(response).to have_http_status(422) }
    end

    # PUT request to update a post
    describe 'PUT /posts/:id' do
      let(:updated_title) { 'This is an updated post' }
      let(:valid_attributes) { { title: updated_title } }

      context 'with invalid headers' do
        before { put "/posts/#{post_id}", params: valid_attributes, headers: invalid_headers }

        it { expect(response).to have_http_status(422) }
      end

      context 'when the record exists' do
        before { put "/posts/#{post_id}", params: valid_attributes, headers: headers }

        it 'updates the post' do
          expect(response.body).to be_empty
        end

        it { expect(response).to have_http_status(204) }
      end
    end

    # DELETE request to delete a post
    describe 'DELETE /posts/:id' do

      context 'with invalid headers' do
        before { delete "/posts/#{post_id}", params: valid_attributes, headers: invalid_headers }

        it { expect(response).to have_http_status(422) }
      end

      context 'with valid headers' do
        before { delete "/posts/#{post_id}", params: {}, headers: headers }

        it { expect(response).to have_http_status(204) }
      end
    end
  end
end
