# Helpers for Controller spec tests
module ControllerSpecHelper
  # Generate tokens from a user id
  def token_generator(user_id)
    JsonWebToken.encode(user_id: user_id)
  end

  # Generate Expired Tokens
  def expired_token_generator(user_id)
    JsonWebToken.encode({ user_id: user_id }, (Time.now.to_i - 100))
  end

  # return Valid Headers
  def valid_headers
    {
      'Authorization' => token_generator(user.id),
      'ContentType' => 'application/json'
    }
  end

  # return Invalid Headers
  def invalid_headers
    {
      'Authorization' => nil,
      'ContentType' => 'application/json'
    }
  end
end
