require 'rails_helper'

RSpec.describe AuthorizeApiRequest do
  subject(:invalid_request_obj) { described_class.new({}) }

  subject(:request_obj) { described_class.new(header) }

  let(:user) { create(:user) }
  let(:header) { { 'Authorization' => token_generator(user.id) } }

  # Entry point for request
  describe '#call' do
    # returns user object when request is valid
    context 'when the request is valid' do
      it 'returns a user object' do
        result = request_obj.call
        expect(result[:user]).to eq(user)
      end
    end

    context 'when the request is invalid' do
      context 'token is missing' do
        it 'raises Missing Token error' do
          expect { invalid_request_obj.call }
            .to raise_error(ExceptionHandler::MissingToken, 'Missing token')
        end
      end

      context 'token is invalid' do
        subject(:invalid_request_obj) do
          described_class.new('Authorization' => token_generator(5))
        end

        it 'raises Invalid Token error' do
          expect { invalid_request_obj.call }
            .to raise_error(ExceptionHandler::InvalidToken, /Invalid token/)
        end
      end

      context 'token has expired' do
        subject(:request_obj) { described_class.new(header) }

        let(:header) { { 'Authorization' => expired_token_generator(user.id) } }

        it 'raises Expired Signature error' do
          expect { request_obj.call }
            .to raise_error(
              ExceptionHandler::ExpiredSignature, /Signature has expired/
            )
        end
      end
    end
  end
end
