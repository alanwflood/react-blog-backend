class AddCreateByToPost < ActiveRecord::Migration[5.1]
  def change
    add_column :posts, :created_by, :integer
  end
end
