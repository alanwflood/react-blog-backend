Rails.application.routes.draw do
  scope module: :v1, constraints: ApiVersion.new('v1', true) do
    resources :posts
  end


  post 'auth/login', to: 'authentication#authenticate'
  post 'signup', to: 'users#create'
  get 'fetch-user', to: 'users#show'

  resources :users do
    get :posts
  end
end
