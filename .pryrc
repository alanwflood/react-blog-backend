if defined?(PryByebug)
  Pry.commands.alias_command 'cn', 'continue'
  Pry.commands.alias_command 'st', 'step'
  Pry.commands.alias_command 'nx', 'next'
  Pry.commands.alias_command 'fn', 'finish'
end