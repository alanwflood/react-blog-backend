# Posts model
class Post < ApplicationRecord
  validates_presence_of :title, :content

  has_many :categories_posts
  has_many :categories, through: :categories_posts
  accepts_nested_attributes_for :categories

  has_attached_file :main_image,
                    styles: {
                      medium: '300x300>',
                      thumb: '100x100>'
                    },
                    default_url: '/images/:style/missing.png'

  validates_attachment_content_type :main_image,
                                    content_type: %r{\Aimage\/.*\z/}
end
