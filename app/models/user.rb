# A General user of our application
class User < ApplicationRecord
  has_secure_password
  has_many :posts, foreign_key: :created_by
  validates_presence_of :first_name, :last_name, :email, :password_digest

  def full_name
    "#{first_name} #{last_name}"
  end
end
