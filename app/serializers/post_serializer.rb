class PostSerializer < ActiveModel::Serializer
  attributes :id, :content, :title, :created_by, :created_at, :updated_at, :main_image
  has_many :categories, through: :categories_posts
end
