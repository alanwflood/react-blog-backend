class CategorySerializer
  has_many :posts, through: :categories_posts
end
