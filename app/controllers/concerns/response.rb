module Response
  extend ActiveSupport::Concern

  # Parse JSON responses
  def json_response(object, status = :ok)
    render json: object, status: status
  end
end
