# frozen_string_literal: true

module V1
  # Handle CRUD methods for Posts
  class PostsController < ApplicationController
    # Skip auth check to get an index of posts and single posts
    skip_before_action :authorize_request, only: %i[index show]

    before_action :retrieve_post, only: %i[show update destroy]

    def index
      @posts = Post.all
      json_response(@posts)
    end

    def show
      json_response(@post)
    end

    def create
      @post = current_user.posts.create!(post_params)
      @post.categories.first_or_create(params[:categories])
      json_response(@post, :created)
    end

    def update
      @post.update(post_params)
      head :no_content
    end

    def destroy
      @post.destroy
      head :no_content
    end

    private

    def post_params
      params.require(:post).permit(
        :title,
        :content,
        :main_image,
        categories: [:names]
      )
    end

    def retrieve_post
      @post = Post.find(params[:id])
    end
  end
end
