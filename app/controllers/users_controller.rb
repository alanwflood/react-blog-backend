# Handle CRUD methods for Users
class UsersController < ApplicationController
  # We can't authorize request cause there's no token yet
  skip_before_action :authorize_request, only: :create

  def create
    user = User.create!(user_params)
    auth_token = AuthenticateUser.new(user.email, user.password)
    response = { message: Message.account_created, auth_token: auth_token }
    json_response(response, :created)
  end

  def show
    user = AuthorizeApiRequest.new(request.headers).call[:user]
    json_response(user)
  end

  def posts
    @user = User.find_by(params[:id])
    json_response(@user.posts)
  end

  private

  def user_params
    params.permit(
      :first_name,
      :last_name,
      :email,
      :password,
      :password_confirmation
    )
  end
end
