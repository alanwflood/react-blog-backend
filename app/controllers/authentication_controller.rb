class AuthenticationController < ApplicationController
  # We can't authorize request cause there's no token yet
  skip_before_action :authorize_request, only: :authenticate

  def authenticate
    auth_user =
      AuthenticateUser.new(auth_params[:email], auth_params[:password]).call
    json_response(auth_user)
  end

  private

  def auth_params
    params.require(:authentication).permit(:email, :password)
  end
end
